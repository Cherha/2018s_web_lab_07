function show() {
    let d = new Date();
    let day = d.getDate();
    let month = d.getMonth() + 1;
    let year = d.getFullYear();
    let hours = d.getHours();
    let min = d.getMinutes();
    let sec = d.getSeconds();
    document.getElementById("time").innerHTML = day + " / " + month + " / " + year + " " + hours + " : " + min + " : " + sec;
}

var interveral = undefined;

function stop() {
    if (interveral !== undefined) {
        clearInterval(interveral);
        interveral = undefined;
    }
}

function start() {
    if (interveral === undefined) {
        interveral = setInterval(show, 1000);
    }
}

window.addEventListener("load", function () {
    document.getElementById("startbutton").addEventListener("click", start);
    document.getElementById("stopbutton").addEventListener("click", stop);
});